# R + PHP

1. Add R to your environment variable path.

    `;C:\R\R-3.2.1`

2. Open your CMD and type,

    `r`

    You should see the info below,

    ```
    Microsoft Windows [Version 6.1.7601]
    Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

    C:\Users\your-user-name>r

    R version 3.2.1 (2015-06-18) -- "World-Famous Astronaut"
    Copyright (C) 2015 The R Foundation for Statistical Computing
    Platform: x86_64-w64-mingw32/x64 (64-bit)

    R is free software and comes with ABSOLUTELY NO WARRANTY.
    You are welcome to redistribute it under certain conditions.
    Type 'license()' or 'licence()' for distribution details.

      Natural language support but running in an English locale

    R is a collaborative project with many contributors.
    Type 'contributors()' for more information and
    'citation()' on how to cite R or R packages in publications.

    Type 'demo()' for some demos, 'help()' for on-line help, or
    'help.start()' for an HTML browser interface to help.
    Type 'q()' to quit R.
    ```

## ref:

    * http://www.r-bloggers.com/integrating-php-and-r/
